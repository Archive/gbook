void prepare_gbook();
void quit_cb(GtkWidget *widget, void *data);
void about_cb(GtkWidget *widget, void *data);
void new_cb(GtkWidget *widget, void *data);
void open_cb(GtkWidget *widget, void *data);
void close_cb(GtkWidget *widget, void *data);
void new_assignment_cb(GtkWidget *widget, void *data, char currentclass);
void new_class_cb(GtkWidget *widget, void *data);
void new_student_cb(GtkWidget *widget, void *data);
void file_sel_ok_cb(GtkWidget *widget, void *data);
void assign_types_wizard_cb(GtkWidget *widget, void *data);
char get_assign_type(char currentclass);


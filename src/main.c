/* This is where config.h should go */
#include <gnome.h>
#include "gbook.h"
#define VERSION "0.0"

GtkWidget *app;
GtkWidget *master_table;

int main(int argc, char *argv[])
{
	gnome_init("gbook", VERSION, argc, argv);

	prepare_gbook();

	gtk_main();
	return(0);
}

static GnomeUIInfo new_submenu [] = {
        GNOMEUIINFO_ITEM("Assignment...", NULL, new_assignment_cb, NULL),
        GNOMEUIINFO_ITEM("Class...", NULL, new_class_cb, NULL),
        GNOMEUIINFO_ITEM("Student...", NULL, new_student_cb, NULL),
        GNOMEUIINFO_END
};

static GnomeUIInfo help_menu [] = {
        GNOMEUIINFO_ITEM_STOCK ("About GBook...", NULL, about_cb,
        GNOME_STOCK_MENU_ABOUT),
        GNOMEUIINFO_END
};

static GnomeUIInfo file_menu [] = {
	GNOMEUIINFO_SUBTREE("New...", &new_submenu),
	GNOMEUIINFO_ITEM_STOCK("Open...", NULL, open_cb, GNOME_STOCK_MENU_OPEN),
	GNOMEUIINFO_ITEM_STOCK("Close", NULL, close_cb, GNOME_STOCK_MENU_CLOSE),
        GNOMEUIINFO_ITEM_STOCK("Exit", NULL, quit_cb, GNOME_STOCK_MENU_EXIT),
        GNOMEUIINFO_END
};

static GnomeUIInfo main_menu [] = {
        GNOMEUIINFO_SUBTREE("File", &file_menu),
	GNOMEUIINFO_JUSTIFY_RIGHT,
        GNOMEUIINFO_SUBTREE("Help", &help_menu),
        GNOMEUIINFO_END
};

void prepare_gbook()
{
	app = gnome_app_new("gbook", "GBook");
	gtk_widget_realize(app);
	gtk_signal_connect(GTK_OBJECT(app), "delete_event",
	GTK_SIGNAL_FUNC(quit_cb), NULL);

	gnome_app_create_menus(GNOME_APP(app), &main_menu);

	master_table = gtk_table_new(1, 1, TRUE);
	gnome_app_set_contents(GNOME_APP(app), GTK_WIDGET(master_table));

	gtk_widget_show(master_table);
	gtk_widget_show(app);
}

void quit_cb(GtkWidget *widget, void *data)
{
	gtk_main_quit();
	return;
}

void about_cb(GtkWidget *widget, void *data)
{
	GtkWidget *about_box;
	gchar *authors[] = {
		"Justin Maurer",
		NULL
	};

	about_box = gnome_about_new("GBook", VERSION, "(C) 1998 Justin Maurer",
			authors, "The GNOME Gradebook", NULL);

	gtk_widget_show(about_box);
	return;
}

void new_cb(GtkWidget *widget, void *data)
{
	GtkWidget *new_dialog_box;
	GtkWidget *new_type_menu;
	GtkWidget *new_type_option_menu;
	GtkWidget *new_type_label;
	GtkWidget *type;
	
	new_dialog_box = gnome_dialog_new("New", GNOME_STOCK_BUTTON_OK, 
		GNOME_STOCK_BUTTON_CANCEL, NULL);
 	
	new_type_menu = gtk_menu_new();

	type = gtk_menu_item_new_with_label("Assignment");
	gtk_menu_append(GTK_MENU(new_type_menu), type);
	gtk_widget_show(type);
	
	type = gtk_menu_item_new_with_label("Class");
	gtk_menu_append(GTK_MENU(new_type_menu), type);
	gtk_widget_show(type);
	
	type = gtk_menu_item_new_with_label("Student");	
	gtk_menu_append(GTK_MENU(new_type_menu), type);
	gtk_widget_show(type);

	new_type_option_menu = gtk_option_menu_new();
	gtk_option_menu_set_menu(GTK_OPTION_MENU(new_type_option_menu), 
			new_type_menu);
	
	new_type_label = gtk_label_new("New");
	
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(new_dialog_box)->vbox),
			new_type_label, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(new_dialog_box)->vbox), 
			new_type_option_menu, TRUE, TRUE, 0);

	gtk_widget_show(new_type_label);
	gtk_widget_show(new_type_option_menu);
	gtk_widget_show(new_type_menu);
	gtk_widget_show(new_dialog_box);
}

void open_cb(GtkWidget *widget, void *data)
{
GtkWidget *file_sel_dialog;

file_sel_dialog = gtk_file_selection_new("Load Class Database");
gtk_file_selection_set_filename(GTK_FILE_SELECTION(file_sel_dialog), "*.cdb");
gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION (file_sel_dialog)->ok_button), 	"clicked", (GtkSignalFunc)file_sel_ok_cb, file_sel_dialog);
gtk_signal_connect_object(
	GTK_OBJECT(GTK_FILE_SELECTION(file_sel_dialog)->cancel_button),
	"clicked", (GtkSignalFunc)gtk_widget_destroy, 
	GTK_OBJECT(file_sel_dialog));
gtk_widget_show(file_sel_dialog);
}

void close_cb(GtkWidget *widget, void *data)
{
}

void new_assignment_cb(GtkWidget *widget, void *data, char currentclass)
{
int i = 0;
int num_assign_types;
/* We can get rid of most of these later, after they've been attached with 
 * gtk object stuff, but for now it's just so we don't have to deal with it */
GtkWidget *new_assignment_dialog;
GtkWidget *new_assignment_table;
GtkWidget *new_assignment_entry_name;
GtkWidget *new_assignment_entry_score;	/* points earned 	*/
GtkWidget *new_assignment_entry_total;  /* out of 100%  	*/
GtkWidget *new_assignment_label;
GtkWidget *new_assignment_type_menu; 
GtkWidget *new_assignment_type_menu_item;
GtkWidget *new_assignment_type_option_menu;
GtkWidget *new_assignment_students_menu;
GtkWidget *new_assignment_students_menu_item;
GtkWidget *new_assignment_students_option_menu;

new_assignment_dialog = gnome_dialog_new("New Assignment", 
		GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
new_assignment_table = gtk_table_new(4, 3, FALSE);
gtk_table_set_row_spacings(GTK_TABLE(new_assignment_table), 15);

new_assignment_label = gtk_label_new("Assignment name: ");
gtk_table_attach_defaults(GTK_TABLE(new_assignment_table),
	new_assignment_label, 0, 1, 0, 1);
gtk_widget_show(new_assignment_label);

new_assignment_entry_name = gtk_entry_new();
gtk_table_attach_defaults(GTK_TABLE(new_assignment_table),
	new_assignment_entry_name, 1, 3, 0, 1);
gtk_widget_show(new_assignment_entry_name);

new_assignment_entry_score = gtk_entry_new();
gtk_table_attach_defaults(GTK_TABLE(new_assignment_table),
        new_assignment_entry_score, 0, 1, 3, 4);
gtk_widget_show(new_assignment_entry_score);

new_assignment_label = gtk_label_new(" points of ");
gtk_table_attach_defaults(GTK_TABLE(new_assignment_table),
	new_assignment_label, 1, 2, 3, 4);
gtk_widget_show(new_assignment_label);

new_assignment_entry_total = gtk_entry_new();
gtk_table_attach_defaults(GTK_TABLE(new_assignment_table),
	new_assignment_entry_total, 2, 3, 3, 4);
gtk_widget_show(new_assignment_entry_total);

new_assignment_label = gtk_label_new("Type: ");
gtk_table_attach_defaults(GTK_TABLE(new_assignment_table),
	new_assignment_label, 0, 1, 2, 3);
gtk_widget_show(new_assignment_label);

new_assignment_type_menu = gtk_menu_new();

for (i = 0; i = 0; i++) // FIXME: should be num_assign_types
{
	new_assignment_type_menu_item = gtk_menu_item_new_with_label(
			(get_assign_type('z')));
	gtk_menu_append(GTK_MENU(new_assignment_type_menu), 
			new_assignment_type_menu_item);
	gtk_widget_show(new_assignment_type_menu_item);
}

new_assignment_label = gtk_label_new("Which students: ");
gtk_table_attach_defaults(GTK_TABLE(new_assignment_table),
	new_assignment_label, 0, 2, 1, 2);
gtk_widget_show(new_assignment_label);

new_assignment_students_menu = gtk_menu_new();
new_assignment_students_menu_item = gtk_menu_item_new_with_label("All");
gtk_menu_append(GTK_MENU(new_assignment_students_menu), 
	new_assignment_students_menu_item);
gtk_widget_show(new_assignment_students_menu_item);
new_assignment_students_menu_item = gtk_menu_item_new_with_label("Selected");
gtk_menu_append(GTK_MENU(new_assignment_students_menu),
        new_assignment_students_menu_item);
gtk_widget_show(new_assignment_students_menu_item);

new_assignment_students_option_menu = gtk_option_menu_new();
gtk_option_menu_set_menu(GTK_OPTION_MENU(new_assignment_students_option_menu),
		new_assignment_students_menu);
gtk_table_attach_defaults(GTK_TABLE(new_assignment_table),
		new_assignment_students_option_menu, 1, 3, 1, 2);
gtk_widget_show(new_assignment_students_option_menu);

new_assignment_type_option_menu = gtk_option_menu_new();
gtk_option_menu_set_menu(GTK_OPTION_MENU(new_assignment_type_option_menu), 
		new_assignment_type_menu);
gtk_table_attach_defaults(GTK_TABLE(new_assignment_table), 
		new_assignment_type_option_menu, 1, 3, 2, 3);
gtk_widget_show(new_assignment_type_option_menu);

gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(new_assignment_dialog)->vbox), 
		new_assignment_table, TRUE, TRUE, 0);

gtk_widget_show(new_assignment_table);
gtk_widget_show(new_assignment_dialog);
}

void new_class_cb(GtkWidget *widget, void *data)
{
GtkWidget *new_class_dialog;
GtkWidget *new_class_table;
GtkWidget *new_class_label;
GtkWidget *new_class_entry_title;
GtkWidget *new_class_entry_semesters;
GtkWidget *new_class_entry_types;

new_class_dialog = gnome_dialog_new("New Class", GNOME_STOCK_BUTTON_OK, 
		GNOME_STOCK_BUTTON_CANCEL, NULL);
new_class_table = gtk_table_new(3, 2, FALSE);
gtk_table_set_row_spacings(GTK_TABLE(new_class_table), 15);
gtk_table_set_col_spacings(GTK_TABLE(new_class_table), 15);

new_class_label = gtk_label_new("Title: ");
gtk_table_attach_defaults(GTK_TABLE(new_class_table), new_class_label, 0, 1, 
	0, 1);
gtk_widget_show(new_class_label);

new_class_entry_title = gtk_entry_new();
gtk_table_attach_defaults(GTK_TABLE(new_class_table), new_class_entry_title, 
		1, 2, 0, 1);
gtk_widget_show(new_class_entry_title);

new_class_label = gtk_label_new("Semesters: ");
gtk_table_attach_defaults(GTK_TABLE(new_class_table), new_class_label, 0, 1,
	1, 2);
gtk_widget_show(new_class_label);

new_class_entry_semesters = gtk_entry_new();
gtk_table_attach_defaults(GTK_TABLE(new_class_table),
		new_class_entry_semesters, 1, 2, 1, 2);
gtk_widget_show(new_class_entry_semesters);

new_class_label = gtk_label_new("# of grade types");
gtk_table_attach_defaults(GTK_TABLE(new_class_table), new_class_label, 0, 1,
	2, 3);
gtk_widget_show(new_class_label);

new_class_entry_types = gtk_entry_new();
gtk_table_attach_defaults(GTK_TABLE(new_class_table), new_class_entry_types,
	1, 2, 2, 3);
gtk_widget_show(new_class_entry_types);

gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(new_class_dialog)->vbox),
	new_class_table, TRUE, TRUE, 0);

gtk_widget_show(new_class_table);
gtk_widget_show(new_class_dialog);
/* FIXME: add some sanity checking for the above text entries */
gnome_dialog_button_connect(GNOME_DIALOG(new_class_dialog), 0, 
		GTK_SIGNAL_FUNC(assign_types_wizard_cb), data);
gnome_dialog_button_connect_object(GNOME_DIALOG(new_class_dialog), 1,
		GTK_SIGNAL_FUNC(gtk_widget_destroy), 
		GTK_OBJECT(new_class_dialog));
/* TODO: add student wizard? */
}

void new_student_cb(GtkWidget *widget, void *data)
{
GtkWidget *new_student_dialog;
GtkWidget *new_student_table;
GtkWidget *new_student_label;
GtkWidget *new_student_entry;
GtkWidget *new_student_check_button;

new_student_dialog = gnome_dialog_new("New Student", GNOME_STOCK_BUTTON_OK,
	GNOME_STOCK_BUTTON_CANCEL, NULL);
new_student_table = gtk_table_new(2, 2, FALSE);

new_student_label = gtk_label_new("Name: ");
gtk_table_attach_defaults(GTK_TABLE(new_student_table), new_student_label,
	0, 1, 0, 1);	
gtk_widget_show(new_student_label);

new_student_entry = gtk_entry_new();
gtk_table_attach_defaults(GTK_TABLE(new_student_table), new_student_entry,
	1, 2, 0, 1);
gtk_widget_show(new_student_entry);

new_student_check_button = gtk_check_button_new_with_label("Inherit existing 
	assignments");
gtk_table_attach_defaults(GTK_TABLE(new_student_table), 
	new_student_check_button, 0, 2, 1, 2);
gtk_widget_show(new_student_check_button);

gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(new_student_dialog)->vbox),
	new_student_table, TRUE, TRUE, 0);			

gtk_widget_show(new_student_table);
gtk_widget_show(new_student_dialog);
}

void file_sel_ok_cb(GtkWidget *widget, void *data)
{
}

char get_assign_type(char currentclass)
{
return 'a'; /* FIXME: for testing */
}

void assign_types_wizard_cb(GtkWidget *widget, void *data)
{
  int i; /* loopcounter */
GtkWidget *assign_types_wizard_dialog;
GtkWidget *assign_types_wizard_table;
GtkWidget *assign_types_wizard_entry;

assign_types_wizard_dialog = gnome_dialog_new("Add Assignment Type", 
		GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
/* shown at bottom */
assign_types_wizard_table = gtk_table_new(3+1, 4, FALSE);
gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(assign_types_wizard_dialog)->vbox),
	assign_types_wizard_table, TRUE, TRUE, 0);      		
gtk_widget_show(assign_types_wizard_table);

 for (i = 0; i<=3; i++) /* should be num_assign_types */
   {
     assign_types_wizard_entry = gtk_entry_new();
     gtk_table_attach_defaults(GTK_TABLE(assign_types_wizard_table), 
			       assign_types_wizard_entry, 1, 2, i, i+1);
     gtk_widget_show(assign_types_wizard_entry);
   }
gtk_widget_show(assign_types_wizard_dialog);
}
